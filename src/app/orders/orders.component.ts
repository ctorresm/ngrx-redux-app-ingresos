import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { IngresoEgresoService } from '../ingreso-egreso/ingreso-egreso.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss'],
})
export class OrdersComponent implements OnInit {
  ingresoSubs: Subscription = new Subscription();
  constructor(private ingresoEgresoService: IngresoEgresoService) {}

  ngOnInit(): void {
    this.ingresoEgresoService.initIngresoEgresoListener();
  }
}

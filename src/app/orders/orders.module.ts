import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrdersRoutingModule } from './orders-routing.module';
import { OrdersComponent } from './orders.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { StoreModule } from '@ngrx/store';
import { IngresoEgresoReducer } from '../ingreso-egreso/ingreso-egreso-reducer';
import { EstadisticaComponent } from '../ingreso-egreso/estadistica/estadistica.component';
import { IngresoEgresoComponent } from '../ingreso-egreso/ingreso-egreso.component';
import { DetalleComponent } from '../ingreso-egreso/detalle/detalle.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    OrdersComponent,
    EstadisticaComponent,
    IngresoEgresoComponent,
    DetalleComponent,
  ],
  imports: [
    CommonModule,
    OrdersRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    ChartsModule,
    RouterModule,
    StoreModule.forFeature('ingresoEgreso', IngresoEgresoReducer),
  ],
})
export class OrdersModule {}

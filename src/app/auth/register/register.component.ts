import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { AppState } from 'src/app/app.reducer';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styles: [],
})
export class RegisterComponent implements OnInit, OnDestroy {
  loading = false;
  suscription: Subscription;

  registerForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    nombre: ['', Validators.required],
    password: ['', Validators.required],
  });

  constructor(
    private fb: FormBuilder,
    public authService: AuthService,
    public store: Store<AppState>
  ) {
    this.suscription = this.store.select('ui').subscribe((ui) => {
      this.loading = ui.isLoading;
    });
  }

  ngOnDestroy(): void {
    this.suscription.unsubscribe();
  }

  ngOnInit(): void {}

  registrar(): void {
    const { email, nombre, password } = this.registerForm.getRawValue();
    this.authService.crearUsuario(nombre, email, password);
  }
}

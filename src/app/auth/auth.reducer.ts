import { createReducer, on } from '@ngrx/store';
import { setUser, unsetUser } from './auth.actions';
import { User } from './user.model';

const initState: AuthState = {
  authenticated: false,
  user: {
    nombre: '',
    email: '',
    uid: '',
  },
};

export const AuthReducer = createReducer(
  initState,
  on(unsetUser, () => {
    return {
      authenticated: false,
      user: initState.user,
    };
  }),
  on(setUser, (state = initState, { user }) => {
    return {
      authenticated: true,
      user: {
        ...user,
      },
    };
  })
);

export interface AuthState {
  authenticated: boolean;
  user: User;
}

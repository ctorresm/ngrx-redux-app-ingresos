import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import Swal from 'sweetalert2';
import { map } from 'rxjs/operators';
import { User } from './user.model';

import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Store } from '@ngrx/store';
import { AppState } from '../app.reducer';
import { removeLoading, setLoading } from '../shared/ui.actions';
import { setUser, unsetUser } from './auth.actions';
import { Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  userSubscription: Subscription = new Subscription();
  usuario: User;

  constructor(
    public auth: AngularFireAuth,
    public dbStore: AngularFirestore,
    private router: Router,
    private store: Store<AppState>
  ) {}

  initAuthListener() {
    this.auth.authState.subscribe((firebaseUser) => {
      if (firebaseUser) {
        this.userSubscription = this.dbStore
          .doc(`${firebaseUser.uid}/usuario`)
          .valueChanges()
          .subscribe((userObj: any) => {
            const newUser = new User(userObj);
            const action = setUser({ user: userObj });
            this.store.dispatch(action);
            this.usuario = newUser;
          });
      } else {
        this.usuario = null;
        this.userSubscription?.unsubscribe();
      }
    });
  }

  crearUsuario(nombre: string, email: string, password: string) {
    this.store.dispatch(setLoading());
    this.auth
      .createUserWithEmailAndPassword(email, password)
      .then((resp) => {
        const user: User = {
          uid: resp.user?.uid,
          nombre,
          email,
        };
        this.dbStore
          .doc(`${user.uid}/usuario`)
          .set(user)
          .then(() => {
            this.store.dispatch(removeLoading());
            this.router.navigate(['/']);
          });
      })
      .catch((error) => {
        Swal.fire('Error!', error.message, 'error');
      });
  }

  loginUsuario(email: string, password: string) {
    this.store.dispatch(setLoading());
    this.auth
      .signInWithEmailAndPassword(email, password)
      .then((resp) => {
        this.store.dispatch(removeLoading());
        this.router.navigate(['/orders']);
      })
      .catch((error) => {
        this.store.dispatch(removeLoading());
        Swal.fire('Error!', error.message, 'error');
      });
  }

  logout() {
    this.router.navigate(['/login']);
    this.auth.signOut();
    this.store.dispatch(unsetUser());
  }

  isAuthenticated() {
    return this.auth.authState.pipe(
      map((fbUser) => {
        if (fbUser === null) {
          this.router.navigate(['/login']);
        }
        return fbUser !== null;
      })
    );
  }

  getUsuario() {
    return { ...this.usuario };
  }
}

import { createReducer, on } from '@ngrx/store';
import { removeLoading, setLoading } from './ui.actions';

export interface State {
  isLoading: boolean;
}

const initState: State = {
  isLoading: false,
};

export const UiReducer = createReducer(
  initState,
  on(setLoading, (state) => {
    return {
      isLoading: true,
    };
  }),
  on(removeLoading, (state) => {
    return {
      isLoading: false,
    };
  })
);

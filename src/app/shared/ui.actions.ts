import { createAction, props } from '@ngrx/store';

export const setLoading = createAction('[UI Loading] Cargando...');

export const removeLoading = createAction(
  '[UI Loading] Desactivar cargando...'
);

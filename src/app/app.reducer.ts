import { ActionReducerMap } from '@ngrx/store';
import { AuthReducer, AuthState } from './auth/auth.reducer';
import { State, UiReducer } from './shared/ui.reducer';

export interface AppState {
  ui: State;
  auth: AuthState | undefined;
}

export const appReducers: ActionReducerMap<AppState> = {
  ui: UiReducer,
  auth: AuthReducer,
};

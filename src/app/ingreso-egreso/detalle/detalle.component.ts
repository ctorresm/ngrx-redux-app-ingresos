import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { AppState } from 'src/app/app.reducer';
import { IngresoEgreso } from '../ingreso-egreso.model';
import { IngresoEgresoService } from '../ingreso-egreso.service';
import Swal from 'sweetalert2';
import { AppStateIE } from '../ingreso-egreso-reducer';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styles: [],
})
export class DetalleComponent implements OnInit, OnDestroy {
  items: IngresoEgreso[] = [];
  subscription: Subscription = new Subscription();

  constructor(
    private store: Store<AppStateIE>,
    private ingresoEgresoService: IngresoEgresoService
  ) {}

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngOnInit(): void {
    this.subscription = this.store
      .select('ingresoEgreso')
      .subscribe((coleccion: any) => {
        this.items = coleccion.items;
      });
  }

  btnBorrarItem(item: IngresoEgreso) {
    this.ingresoEgresoService.borrarIngresoEgreso(item.uid).then((res) => {
      Swal.fire(
        '¡Correcto!',
        item.descripcion + ' eliminado correctamente',
        'success'
      );
    });
  }
}

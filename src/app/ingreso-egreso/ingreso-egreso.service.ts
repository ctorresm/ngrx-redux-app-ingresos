import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from '../auth/auth.service';
import { IngresoEgreso } from './ingreso-egreso.model';
import Swal from 'sweetalert2';
import { Observable, Subscription } from 'rxjs';
import { AppState } from '../app.reducer';
import { Store } from '@ngrx/store';
import { filter, map } from 'rxjs/operators';
import { setItems, unsetItems } from './ingreso-egreso-actions';

@Injectable({
  providedIn: 'root',
})
export class IngresoEgresoService {
  ingresoEgresoListener$: Subscription = new Subscription();
  ingresoEgresoItems$: Subscription = new Subscription();

  constructor(
    private dataBase: AngularFirestore,
    private authService: AuthService,
    private store: Store<AppState>,
    public dbStore: AngularFirestore
  ) {}

  crearIngresoEgreso(ingresoEgreso: IngresoEgreso) {
    const user = this.authService.getUsuario();
    return this.dataBase
      .doc(`${user.uid}/ingresos-egresos`)
      .collection('items')
      .add({ ...ingresoEgreso });
  }

  ingresoEgresoItems(uid: string) {
    this.ingresoEgresoItems$ = this.dbStore
      .collection(`${uid}/ingresos-egresos/items`)
      .snapshotChanges()
      .pipe(
        map((doc) => {
          return doc.map((d) => {
            const obj: any = d.payload.doc.data();
            return {
              uid: d.payload.doc.id,
              ...obj,
            };
          });
        })
      )
      .subscribe((coleccion: IngresoEgreso[]) => {
        const action = setItems({ items: coleccion });
        this.store.dispatch(action);
      });
  }

  initIngresoEgresoListener() {
    this.ingresoEgresoListener$ = this.store
      .select('auth')
      .pipe(filter((auth) => auth.user.nombre !== ''))
      .subscribe((auth) => {
        this.ingresoEgresoItems(auth.user.uid);
      });
  }

  cancelarSubscripciones() {
    this.ingresoEgresoItems$.unsubscribe();
    this.ingresoEgresoListener$.unsubscribe();
    this.store.dispatch(unsetItems());
  }

  borrarIngresoEgreso(uid: string) {
    const user = this.authService.getUsuario();
    return this.dataBase
      .doc(`${user.uid}/ingresos-egresos/items/${uid}`)
      .delete();
  }
}

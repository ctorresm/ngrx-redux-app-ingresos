import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IngresoEgreso } from './ingreso-egreso.model';
import Swal from 'sweetalert2';
import { IngresoEgresoService } from './ingreso-egreso.service';
import { Store } from '@ngrx/store';
import { AppState } from '../app.reducer';
import { Subscription } from 'rxjs';
import { removeLoading, setLoading } from '../shared/ui.actions';
import { AppStateIE } from './ingreso-egreso-reducer';
@Component({
  selector: 'app-ingreso-egreso',
  templateUrl: './ingreso-egreso.component.html',
  styles: [],
})
export class IngresoEgresoComponent implements OnInit, OnDestroy {
  registerForm: FormGroup;
  tipo = 'ingreso';

  loadingSubs: Subscription = new Subscription();
  cargando = false;

  constructor(
    private fb: FormBuilder,
    private ingresoEgresoService: IngresoEgresoService,
    private store: Store<AppStateIE>
  ) {
    this.registerForm = this.fb.group({
      descripcion: ['', Validators.required],
      monto: [0, Validators.required],
    });
  }

  ngOnDestroy(): void {
    this.loadingSubs.unsubscribe();
  }

  ngOnInit(): void {
    this.loadingSubs = this.store
      .select('ui')
      .subscribe((ui) => (this.cargando = ui.isLoading));
  }

  saveTransaction(): void {
    this.store.dispatch(setLoading());
    const ingresoEgreso = new IngresoEgreso({
      ...this.registerForm.getRawValue(),
      tipo: this.tipo,
    });
    this.ingresoEgresoService.crearIngresoEgreso(ingresoEgreso).then((res) => {
      this.store.dispatch(removeLoading());
      this.registerForm.reset({
        monto: 0,
      });
      Swal.fire('Creado correctamente', ingresoEgreso.descripcion, 'success');
    });
  }
}

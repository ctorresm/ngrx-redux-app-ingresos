import { createReducer, on } from '@ngrx/store';
import { AppState } from '../app.reducer';
import { setItems, unsetItems } from './ingreso-egreso-actions';
import { IngresoEgreso } from './ingreso-egreso.model';

export interface IngresoEgresoState {
  items: IngresoEgreso[];
}

const initState: IngresoEgresoState = {
  items: [],
};

export interface AppStateIE extends AppState {
  ingresoEgreso: IngresoEgresoState;
}

export const IngresoEgresoReducer = createReducer(
  initState,
  on(unsetItems, (state) => {
    return {
      items: [],
    };
  }),
  on(setItems, (state, { items }) => {
    return {
      items: [...items],
    };
  })
);
